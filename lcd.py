#!/usr/bin/python3

import math
import time
from time import sleep

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from flask import Flask, render_template, redirect, request, url_for

import RPi.GPIO as GPIO

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

app = Flask(__name__)


class PiBuzzer:
    def __init__(self, buzzer_pin):
        self.pin = buzzer_pin
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin, GPIO.OUT)

    def buzz(self, pitch, duration):
        if(pitch == 0):
            time.sleep(duration)
            return

        period = 1.0 / pitch
        delay = period / 2
        cycles = int(duration * pitch)

        for i in range(cycles):
            GPIO.output(self.pin, True)
            time.sleep(delay)
            GPIO.output(self.pin, False)
            time.sleep(delay)


class PiDisplay:
    def __init__(self):
        # RPI Pin config
        self.RST = 24
        # 128x32 display
        self.disp = Adafruit_SSD1306.SSD1306_128_32(rst=self.RST)
        # Create display and setup configuration
        self.disp.begin()
        self.width = self.disp.width
        self.height = self.disp.height
        self.disp.clear()
        self.disp.display()
        # Create an image buffer
        self.image = Image.new('1', (self.width, self.height))

        # Create the alert buzzer
        self.buzzer = PiBuzzer(13)

        # Load font to use with display
        self.font = ImageFont.load_default()
        self.draw = ImageDraw.Draw(self.image)

    def draw_text(self, text):
        self.draw.rectangle((0,0,self.width,self.height), outline=0, fill=0)
        self.draw.text((10, 10), text, font=self.font, fill=255)
        self.disp.image(self.image)
        self.disp.display()
        self.buzzer.buzz(1000, 2)

disp = PiDisplay()
disp.draw_text("Initialized")

@app.route("/")
def index():
    return render_template("index.html",
                           title='Home')

@app.route('/', methods=['POST'])
def get_input():
    text = request.form['input']
    disp.draw_text(text)
    return redirect(url_for('index'))

if __name__ == '__main__':
    # Start flask
    app.run(host='0.0.0.0', port=80)
